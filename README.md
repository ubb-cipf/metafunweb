![Image](https://bioinfo.cipf.es/metafun/assets/images/metafun.jpeg)


# Summary

The existence of sex and gender differences in Biomedicine has been thoroughly acknowledged in the literature, and yet, very scarcely analyzed. There exists a growing amount of datasets stored in public data sources that have not been analyzed with a sex/gender perspective but which include the necessary information to do it. MetaFun allows to analyze different kinds of omics data with a sex perspective and to combine different datasets to gain major statistical power and biological soundness.


This web tool has been created with Angular in order to let MetaFun grow easily in the near future. All the meta-analysis scripts have been developed using R meanwhile the middleware has been written in Java. MetaFun executes all the scripts for the analysis server-side, as we have a powerful cluster, the results are quick, and the data can be stored for further analysis if the user decides so. All the results are not only easy-readable but interactive too. To make this possible, most of the graphics are generated with Plotly so the researchers can obtain the information in the best possible way.


The main objective of MetaFun is to allow every single interested researcher in the community to use meta-analysis in their own investigations aiming to a better comprehension of their results.



# Files Format


### Expression Matrix:
In order to process the files, we need first to have two files for each study, the first one the **normalized** expression matrix in CSV format, with the sample names as column names and the ENTREZ_ID as row names as in the example below.
```{text_plain}
"","GSM254625","GSM254626","GSM254627","GSM254628"
"780",10.920058985,9.9536802285,10.44761311,9.731050093
"5982",6.948866629,6.81502139,7.543393412,6.6331248175
"3310",8.7864846495,8.0987384915,8.130723065,8.154441737
"7849",6.457861687,6.4239490915,6.8522412885,6.591329736
"2978",6.107564333,6.170746902,6.501881283,6.397340988
"7318",8.8617739295,9.4044746405,8.452137633,8.9610769475
"7067",5.884616074,6.260078988,6.213472704,6.284604848
"11099",5.723299085,6.0717375655,5.7520701475,6.467311497
"6352",8.848925254,8.7337860585,9.060863672,8.731077027

```

### Experimental Design:
The second file is the experimental design for each study. We need to know which sample is Case, Control and/or Male, Female. This file must be in TSV format so, the format should be:

```{text_plain}
GSM254625	Adenocarcinoma_Male
GSM254626	Control_Male
GSM254627	Adenocarcinoma_Male
GSM254628	Control_Male
GSM254629	Adenocarcinoma_Female
GSM254630	Adenocarcinoma_Male
GSM254633	Adenocarcinoma_Male
GSM254634	Control_Male
GSM254639	Adenocarcinoma_Female
GSM254640	Control_Female
```
**The separator character for the experimental design files is a tabulation (\t)**

# Analysis Options
### Fixed Effect Model or Random Effect Model?

The fixed effect model is useful when you have designed and made all the studies with the same technology, platform and in similar times, meanwhile, the random effect model allows to compute more variability so, if you are integrating several studies with different designs and platform, the random effect model is the best choice.

### GSA or Hipathia?
<div style="text-align: right;">
In MetaFun you can obtain the functional characterization of the genes with two different tools, on one hand, you have GSA, Gene Set Analysis. This will result in a list of infra or overrepresented GO terms which will be meta-analyzed.
<br>
On the other hand, you can run functional profiling with Hipathia. <br>
Hipathia is a method for the computation of signal transduction along signaling pathways from transcriptomic data. <br>The method is based on an iterative algorithm that is able to compute the signal intensity passing through the nodes of a network by taking into account the level of expression of each gene and the intensity of the signal arriving at it. It also provides a new approach to functional analysis allowing to compute the signal arriving at the functions annotated to each pathway.
</div>

### Should I use Propagate option?
<div style="text-align: right;">
If you use the propagate option for the GSA analysis you will obtain <strong>more</strong> GO terms but <strong>less</strong> specific.
If you choose not to propagate the results of the GSA analysis will be <strong>more</strong> specific GO terms but <strong>less</strong> amount of them

</div>

# Results:

### Analysis Summary:
In this tab we provide the information of the launched job and a quantitative summary of the results, we offer you how many samples of each condition the studies had, how many genes and GO terms or signaling pathways have resulted differentially expressed, and how many of them have resulted significant when the meta-analysis have been complete.

### Exploratory Analysis:
This section allows us to know if the data we had were good enough and we can assure that the conclusions of the meta-analysis are relevant. The Exploratory Analysis tab is composed of three different types of graphics:

**BoxPlots**: this kind of plot represents the variability of the data, in terms of their three quartiles, in the bottom of each Box we have the first quartile, the top of the box represents the third quartile, and the line that crosses the box its the median of the data. Furthermore, we have to lines that came up from the bottom and the top of each box the end of these lines marks the maximum and the minimum of the data.

**PCA Plot**: We run a principal component analysis and present you the first two components for your data in the order you can visualize how the samples group up.

**Cluster Plot**: Pretty much the same we do with the PCA we do it with clustering techniques, this way you can ban the sample you have had trouble with just because of reasons

### Differential Expression:
We do a differential expression analysis from your normalized data that can result into significant results or not, this question is not important because, we run this analysis in order to get the genes that we are going to functionally characterize, but you can download them anyways.

### Functional Profiling:
After the differential expression analysis, the genes are evaluated with the functional characterization option you chose at the beginning of the analysis, this is the data we are meta-analyzing.
You can navigate the results and download them the same way you are able to do it with the above analysis.

### Meta-Analysis Results:
Here you will have the final results, in this tab, you will be able to navigate the meta-analysis results. We provide you with two types of graphics:

**Forest Plot**:
In this type of plot represents in the horizontal axis the Log Odds Ratio value of the selected function and its confidence interval. The red line under the X axis represents the summary LOR of the function among all studies.

**Funnel Plot**:
In a Funnel Plot we have represented the variability (Standard Error) in the Y axis, meanwhile in de X axis we have the efect size, LOR in this case. If we have a studies that is represented outside of the margins repeatedly we should consider eliminate it from the metaanalysis.
