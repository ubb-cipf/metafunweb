import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FileUploader} from 'ng2-file-upload';
import axios, {} from 'axios';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-file-browser',
  templateUrl: './file-browser.component.html',
  styleUrls: ['./file-browser.component.css']
})
export class FileBrowserComponent implements OnInit {
  user: string;
  files;
  backFolder;
  currentFolder: string;
  pathBar;
  item: any;
  public uploader: FileUploader = new FileUploader({url: '//bioinfo.cipf.es/ws-metafun/files/ngfileupload'});
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  @Output() selectedFile = new EventEmitter<string>();
  @Input() fileType: string;

  constructor(public ngxSmartModalService: NgxSmartModalService, private cookieService: CookieService, private http: HttpClient) {
  }

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    this.backFolder = [];
    this.currentFolder = 'files';
    this.pathBar = document.getElementById('filesPath');
    this.pathBar.innerText = this.currentFolder + '/';
    const backButton = document.getElementById('backButton');
    const reloadButton = document.getElementById('reloadButton');
    const createFolderButton = document.getElementById('addFolderButton');
    backButton.addEventListener('click', (e: Event) => this.goBack());
    reloadButton.addEventListener('click', (e:Event) => this.loadUserFiles());
    createFolderButton.addEventListener('click', (e: Event) => this.createFolder());
    this.loadUserFiles();
  }

  async loadUserFiles() {
    const api = new ApiUtils('files');
    const resp = await api.getFilesFromUser(this.user);
    if (resp.length > 0) {
      this.files = JSON.parse(resp);
    }
    this.createTable(this.files);
  }
  checkEmptyTextbox() {
    const folNamein: HTMLInputElement = document.getElementById('folderName') as HTMLInputElement;
    const folButton: HTMLButtonElement = document.getElementById('addFolderButton') as HTMLButtonElement;
    const folN = folNamein.value.trim();
    if (folN.length > 0) {
      folButton.disabled = false;
    } else {
      folButton.disabled = true;
    }

  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  createTable(files: Array<string>) {
    this.cleanTable();
    files = files.sort(function compareTypes(a, b) {
      // @ts-ignore
      if (a.type.match('FOLDER')) {
        return 1;
      }
      // @ts-ignore
      if (b.type.match('FOLDER')) {
        return -1;
      }
    });

    if (files.length > 0) {
      const folders = [];
      const regFiles = [];
      const numTerms = files.length;
      const table = document.getElementById('fileManager') as HTMLDivElement;
      let row = '';
      let name = '';
      let allRows = '';
      let splitedPath = [];
      for (let i = 0; i < numTerms; i++) {
        // @ts-ignore
        splitedPath = files[i].path.split('/');
        const fatherFolder = splitedPath[splitedPath.length - 2];
        // @ts-ignore
        if (fatherFolder.match(this.currentFolder.split('/').pop())) {
          // @ts-ignore
          name = files[i].name;
          if ((name.length) > 15) {
            // @ts-ignore
            name = files[i].name.substring(0, 12) + '...';
          }
          row = '<div class="col s2 fileManagerItem center">';
          // @ts-ignore
          if (files[i].type.match('FOLDER')) {
            // @ts-ignore
            if (files[i].name != "MyFolder") {
              row += '<p> <i name=deleteFolderButton class="material-icons tiny right" style="cursor: pointer;">delete_forever</i>\
              <i name=editFolderNameButton class="material-icons tiny right" style="cursor: pointer;">edit</i> \
              <i name=folderButtonShow class="material-icons large center" style="cursor: default; \
                        color: #9AB3C1;">folder_open</i></p>';
            }
            else {
              row += '<p> <i class="material-icons tiny right" style="cursor: pointer;">not_interested</i>\
              <i name=folderButtonShow class="material-icons large center" style="cursor: default; \
                        color: #9AB3C1;">folder_open</i></p>';
            }
            row += '<label>\n' +
              '      <span style="color: #454545">' + name + '</span>\n' +
              '    </label>';
            folders.push(files[i]);
          } else {
            // @ts-ignore
            if (files[i].bioFormat.match('expressionMatrix')) {
              row += '<p><i name=filesButtonShow class="material-icons large center" style="cursor: default; \
              color: #9AB3C1;">assignment</i></p>';
              // row += '<i name=filesButtonShow class="material-icons large center" style="cursor: default; margin-top: 1vh;"><img\n' +
              //   '            src="../../metafun/assets/images/csv_grey.png" height="70" width="65"/></i><p></p>';
              row += '<label>\n' +
                '     <span style="color: #454545;">' + name + '</span>\n' +
                '    </label>';
              regFiles.push(files[i]);
            } else {
              row += '<p><i name=filesButtonShow class="material-icons large center" style="cursor: default; \
              color: #9AB3C1;">note_alt</i></p>';
              // row += '<i name=filesButtonShow class="material-icons large center" style="cursor: default;  margin-top: 2vh;"><img\n' +
              //   '            src="../../metafun/assets/images/ddf_grey.png" height="70" width="51"/></i><p></p>';
              row += '<label>\n' +
                '      <span style="color: #454545">' + name + '</span>\n' +
                '    </label>';
              regFiles.push(files[i]);
            }
          }
          row += '</div>';
          allRows += row;
        }
      }
      table.innerHTML += allRows;
      let b = document.getElementsByName('folderButtonShow');
      for (let i = 0; i < b.length; i++) {
        // @ts-ignore
        b[i].addEventListener('dblclick', (e: Event) => this.openFolder(folders[i].name, true));
      }
      b = document.getElementsByName('filesButtonShow');
      for (let i = 0; i < b.length; i++) {
        // @ts-ignore
        b[i].addEventListener('click', (e: Event) => this.filesShowButton(regFiles[i]));
      }
      b = document.getElementsByName('deleteFolderButton');
      for (let i = 0; i< b.length; i++) {
        b[i].addEventListener('click', (e: Event) => this.deleteFolder(folders[i]));
      }
    }
  }

  editFolderNameButton(folderIndex) {
    
  }

  deleteFolder(folder) {
    if (confirm("This will delete the folder and all it's content\n Are you sure?")) {
      const api = new ApiUtils('files');
      api.deleteFolder(folder.user, folder.path);
      this.loadUserFiles()
    }
  }

  async createFolder() {
    const elem = document.getElementById('folderName') as HTMLInputElement;
    const api = new ApiUtils('files');
    let folderName = this.currentFolder + '/' + elem.value;
    folderName = folderName.replace(/\s/g, '_');
    if (!this.containsSpecialChars(folderName)) {
      const res = await api.createFolder(this.user, folderName, this.currentFolder);
      elem.value = '';
      await this.loadUserFiles();
    } else {
      console.log (folderName)
      alert('Please, do not use special characters')
    }

  }
  containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()+\-=\[\]{};':"\\|,.<>?~àáâãäåæçèéêëìíîïñòóôõöœùúûüýÿÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÑÒÓÔÕÖŒÙÚÛÜÝŸ]/;
    return specialChars.test(str);
  }
  cleanTable() {
    const table = document.getElementById('fileManager') as HTMLDivElement;
    table.innerHTML = '';
  }
  omit_special_char(event) {
    let k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return((k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32 || (k >= 48 && k <= 57));
  }
  async openFolder(folder: string, logChange: boolean) {
    if (folder.match('files') || folder.match('files/')) {
      await this.loadUserFiles();
      this.backFolder = [];
      this.currentFolder = 'files';
      this.pathBar.innerText = 'files/';
    } else {
      if (logChange) {
        this.backFolder.push(this.currentFolder);
        this.currentFolder += '/' + folder;
      }
      this.pathBar.innerText = this.currentFolder;
      const api = new ApiUtils('files');
      const folderContent = await api.getFolderContent(this.user, this.currentFolder);
      let myParsed = [];
      if (folderContent.length > 0) {
        myParsed = JSON.parse(folderContent);
      }
      this.createTable(myParsed);
    }
  }

  async goBack() {
    if (this.backFolder.length > 0) {
      const item = this.backFolder.pop();
      this.currentFolder = item;
      const goTo = item.split('/').pop();
      await this.openFolder(goTo, false);
    }
  }

  filesShowButton(file) {
    this.pathBar.innerText = this.currentFolder + '/' + file.name;
    this.ngxSmartModalService.setModalData(file, 'infoModal');
    this.selectedFile.emit(file);
  }

  async uploadFiles(item: any) {
    const api = new ApiUtils('files');
    const a = document.getElementById('techInput' + item.file.name) as HTMLInputElement;
    const b = this.axiosUpload(item.file.rawFile, this.user, item.file.name.replace(/\s/g, '_'), 'FILE', a.value, this.currentFolder, this.currentFolder);
    await this.loadUserFiles();
  }

  async axiosUpload(fileSend: File, user: string, nameFile: string, typeFile: string, tech: string, path: string, parent: string) {
    const upData = new FormData();
    upData.append('file', fileSend);
    upData.append('user', user);
    upData.append('nameFile', nameFile);
    upData.append('type', typeFile);
    upData.append('tech', tech);
    upData.append('path', path);
    upData.append('parent', parent);
    axios.defaults.headers['Access-Control-Allow-Origin'] = '*';
    axios.post('https://bioinfo.cipf.es/ws-metafun/files/uploadWithPost', upData, {
      onUploadProgress: (progressEvent) => {
        if (progressEvent.lengthComputable) {
          this.updateProgressBarValue(progressEvent, nameFile);
        }
      }
    }).then(
      response => {
        this.loadUserFiles();
      } // if the response is a JSON object // Handle the success response object
    ).catch(
      error => console.log(error) // Handle the error response object
    );
  }

  updateProgressBarValue(progressEvent: any, itemname: any) {
    const progress = progressEvent.loaded / progressEvent.total;
    document.getElementById('progressBar' + itemname).setAttribute('style', 'width: ' + progress * 100 + '%');
  }

  fileDroped($event: File[]) {
  }

  async ableButton(item: any) {
    const button = document.getElementById('uploadFilesButton' + item.file.name) as HTMLButtonElement;
    button.disabled = false;
  }
}
