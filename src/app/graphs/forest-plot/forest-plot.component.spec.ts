import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForestPlotComponent } from './forest-plot.component';

describe('ForestPlotComponent', () => {
  let component: ForestPlotComponent;
  let fixture: ComponentFixture<ForestPlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForestPlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForestPlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
