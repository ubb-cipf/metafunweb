import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import { R3TargetBinder } from '@angular/compiler';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit, OnChanges {
  stdNames;
  user;
  designHeader;
  public useResizeHandler = true;
  summaryDesign;
  studyNames = [];
  @Input() selectedAnalysis;
  private _selectedAnalysis;
  stdCount = 0;
  diffExpHeader: any;
  totalSamples = [];
  contrast: any;
  summaryDiffExp: any;
  funcProfileHeader: any;
  summaryFuncProfile: any;
  metaHeader: any;
  summaryMeta: any;
  public summaryBarplot = {
    data: [],
    layout: {
      title: 'Sample composition per study',
      showlegend: true,
      barmode: 'group',
      paper_bgcolor: 'rgba(0,0,0,0)',
      plot_bgcolor: 'rgba(0,0,0,0)',
      legend: {"orientation": "h", x: 0, y: -0.25}
    }
  };
  constructor(private cookieService: CookieService) { }

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
  }

  async ngOnChanges(changes: SimpleChanges) {
    const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    this._selectedAnalysis = selectedAnalysis.currentValue;
    this.totalSamples = [];
    this.summaryBarplot = {
      data: [],
      layout: {
        title: 'Sample composition per study',
        showlegend: true,
        barmode: 'group',
        paper_bgcolor: 'rgba(0,0,0,0)',
        plot_bgcolor: 'rgba(0,0,0,0)',
        legend: {"orientation": "h", x: 0, y: -0.25}
      }
    };
    if (this._selectedAnalysis !== undefined && !this._selectedAnalysis.match('undefined')) {
      const stdNamess = [];
      const api = new ApiUtils('files');
      const summariesResponse = await api.getSummaries(this.user, this.selectedAnalysis);
      this.contrast = await api.getJobDesign(this.user, this.selectedAnalysis);
      sessionStorage.setItem('RESULTS_CONTRAST', this.contrast);
      sessionStorage.setItem('SELECTED_ANALYSIS', this._selectedAnalysis);
      const re = /_/gi;
      const splitedSummaries = summariesResponse.split('#');

      const rawDesign = splitedSummaries[0];
      const rawDifExp = splitedSummaries[1];
      const rawFunPro = splitedSummaries[2];
      let rawMeta = '';
      if (splitedSummaries.length === 5) {
        rawMeta = splitedSummaries[3];
      }
      if (splitedSummaries.length === 6) {
        rawMeta = splitedSummaries[3] + splitedSummaries[4].split('\n')[2];
           }
      if (splitedSummaries.length === 7) {
        rawMeta = splitedSummaries[3] + splitedSummaries[4].split('\n')[2] + '\n' + splitedSummaries[5].split('\n')[2] + '\n';
           }

      // const rawMeta = splitedSummaries[3] + splitedSummaries[4].split('\n')[2] + '\n' + splitedSummaries[5].split('\n')[2] + '\n';
      this.designHeader = rawDesign.split('\n')[0].split(',');
      this.designHeader[0] = 'Study Name';
      for (let i = 0 ; i < this.designHeader.length; i++) {
        this.designHeader[i] = this.designHeader[i].replace(re, ' ');
        this.totalSamples.push(0);
      }
      const auxsummaryDesign = rawDesign.split('\n');
      auxsummaryDesign.pop();
      auxsummaryDesign.shift();
      this.summaryDesign = auxsummaryDesign;
      const traces = [];
      for (let i = 0 ; i < auxsummaryDesign.length; i++) {
        stdNamess.push(auxsummaryDesign[i].split(',')[0]);
        for (let h = 1; h < this.designHeader.length; h++) {
          this.totalSamples[h] += parseInt(auxsummaryDesign[i].split(',')[h]);
        }
      }
      sessionStorage.setItem('RESULTS_STDNAMES', JSON.stringify(stdNamess));
      const colors = ['#FFA500', '#009E73', '#808080', '#56B4E9'];
      // for (let i = 0 ; i < this.summaryDesign.length; i++) {
      //   let tempTrace = {};
      //   const samplesNum = this.summaryDesign[i].split(',');
      //   tempTrace = {
      //     x: this.designHeader.slice(1, 5),
      //     y: samplesNum.slice(1, samplesNum.length),
      //     name: samplesNum[0],
      //     type: 'bar',
      //     marker: {
      //       color: colors[i]
      //     },
      //   };
      //   this.summaryBarplot.data.push(tempTrace);
      // }
      for (let i = 0 ; i < this.designHeader.slice(1, 5).length; i++) {
        const caso = this.designHeader.slice(1, 5)[i];
        const studies = [];
        const values = [];
        for(let j = 0; j < this.summaryDesign.length; j++){
          const samplesNum = this.summaryDesign[j].split(',');
          studies[j] = samplesNum[0];
          values[j] = samplesNum[i+1]
        }
        let tempTrace = {};
        tempTrace = {
          x: studies,
          y: values,
          name: caso, //samplesNum[0],
          type: 'bar',
          marker: {
            color: colors[i]
          },
        };
        this.summaryBarplot.data.push(tempTrace);
      }



      this.diffExpHeader = rawDifExp.split('\n')[1].split(',');
      this.diffExpHeader[0] = 'Study Name';
      const diffexpAux = rawDifExp.split('\n');
      diffexpAux.shift();
      diffexpAux.shift();
      diffexpAux.pop();
      this.summaryDiffExp = diffexpAux;

      this.funcProfileHeader = rawFunPro.split('\n')[1].split(',');
      this.funcProfileHeader[0] = 'Study Name';
      const funcProfAux = rawFunPro.split('\n');
      funcProfAux.shift();
      funcProfAux.shift();
      funcProfAux.pop();
      this.summaryFuncProfile = funcProfAux;

      // this.metaHeader = rawMeta.split('\n')[1].split(',');
      this.metaHeader = [];
      this.metaHeader[0] = 'Ontology';
      this.metaHeader[1] = 'Total Functions';
      this.metaHeader[2] = 'significant Positive (LOR) ';
      this.metaHeader[3] = 'significant Negative (LOR)';
      this.metaHeader[4] = 'Total significant Functions';
      const metaAus = rawMeta.split('\n');
      metaAus.shift();
      metaAus.shift();
      metaAus.pop();
      this.summaryMeta = metaAus;
      }
  }

}
