import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HipathiaTableComponent } from './hipathia-table.component';

describe('HipathiaTableComponent', () => {
  let component: HipathiaTableComponent;
  let fixture: ComponentFixture<HipathiaTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HipathiaTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HipathiaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
