import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import ApiUtils from '../../utils/ApiUtils';

@Component({
  selector: 'app-hipathia-table',
  templateUrl: './hipathia-table.component.html',
  styleUrls: ['./hipathia-table.component.css']
})
export class HipathiaTableComponent implements OnInit, OnChanges {
  private parsedData;
  public pathData;
  private user;
  links = [];
  private stdNames;
  private  _selectedAnalysis;
  @Input() selectedAnalysis;
  @Input() gseaFlag;
  @Input() loadData

  constructor(private cookieService: CookieService) { }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (!this.gseaFlag && this.loadData === 1) {
      this.links = [];
      const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
      this._selectedAnalysis = selectedAnalysis.currentValue;
      const api = new ApiUtils('files');
      const data = await api.getHipathiaData(this.user, this._selectedAnalysis + '/topHipathia1.txt');
      if (this._selectedAnalysis !== 'undefined' && this._selectedAnalysis !== undefined) {
        this.parsedData = JSON.parse(data);
        for (let i = 0 ; i < this.parsedData.data.length; i++) {
          this.links.push('https://www.genome.jp/kegg-bin/show_pathway?select_scale=1.0&query=' +
            this.parsedData.data[i].name.split(':')[1].split(' ')[1] +
            '&map=' + this.parsedData.data[i].pathway.split('-')[1] + '&scale=&orgs=&auto_image=&show_description=hide&multi_query=');
        }
      }
      const selectStydyInput = document.getElementById('hipathiaStudySelect');
      selectStydyInput.addEventListener('change', (e: Event) => this.studySelection());
      if (!(this._selectedAnalysis === undefined) && !(this._selectedAnalysis === 'undefined')) {
        const aux = await api.getStudiesNames(this.user, this._selectedAnalysis);
        this.stdNames = aux.split('\n');
        selectStydyInput.innerHTML = '<option value="" disabled selected>Choose your study</option>';
        for (let i = 0; i < this.stdNames.length - 1; i++) {
          selectStydyInput.innerHTML += '<option value="' + i + '">' + this.stdNames[i].split('\t')[1].toUpperCase() + '</option>';
        }
      }
      this.pathData = this.parsedData.data;
    }
  }

  async studySelection() {
    if (!this.gseaFlag) {
      const studyIndex = document.getElementById('hipathiaStudySelect') as HTMLOptionElement;
      const api = new ApiUtils('files');
      const data = await api.getHipathiaData(this.user, this._selectedAnalysis + '/topHipathia' + (+studyIndex.value + 1)  + '.txt');
      this.parsedData = JSON.parse(data);
      this.pathData = [];
      this.pathData = this.parsedData.data;
    }
  }
}
