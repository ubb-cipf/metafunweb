import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterplotComponent } from './clusterplot.component';

describe('ClusterplotComponent', () => {
  let component: ClusterplotComponent;
  let fixture: ComponentFixture<ClusterplotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClusterplotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterplotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
