import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {saveAs} from '../../../../node_modules/file-saver';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-diff-expresion',
  templateUrl: './diff-expresion.component.html',
  styleUrls: ['./diff-expresion.component.css']
})
export class DiffExpresionComponent implements OnInit, OnChanges {
  private static orderedbyLor = 1;
  private static orderedbypVal = 1;
  private static orderedbyadj = 1;
  @Input() selectedAnalysis: string;
  @Input() loadData = 0;
  private user;
  private tableData;
  private studyIndex = 0;
  // tslint:disable-next-line:variable-name
  private _selectedAnalysis: string;
  constructor(private cookieService: CookieService) { }
  stdNames: string[] = ['NA', 'NA'];
  searchValDif: any;
  searchPValDif: any;

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    document.getElementById('downloadDiffExpButton').addEventListener('click', (e: Event) => this.downloadDifExp());
    // this.getPcaPlotValues();
  }
  async ngOnChanges(changes: SimpleChanges) {
    if (this.loadData === 1) {
        this._selectedAnalysis = sessionStorage.getItem('SELECTED_ANALYSIS');
        const api = new ApiUtils('files');
        let count = 0;
        // const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
        const selectStydyInput = document.getElementById('studySelect');
        selectStydyInput.addEventListener('change', (e: Event) => this.studieSelection());

        // this._selectedAnalysis = selectedAnalysis.currentValue;
        if (!(this._selectedAnalysis === undefined) && !(this._selectedAnalysis === 'undefined')) {
            // const aux = await api.getStudiesNames(this.user, this._selectedAnalysis);
            this.stdNames = JSON.parse(sessionStorage.getItem('RESULTS_STDNAMES'));
            const sCount = await api.getPlotCount(this.user, this._selectedAnalysis);
            count = +sCount;
            selectStydyInput.innerHTML = '<option value="" disabled selected>Choose your study</option>';
            for (let i = 0; i < this.stdNames.length; i++) {
                selectStydyInput.innerHTML += '<option value="' + i + '">' + this.stdNames[i].toUpperCase() + '</option>';
            }
            this.getDiffExpData(0);
        }
    }
  }

  studieSelection() {
    const studyHTMLIndex = document.getElementById('studySelect') as HTMLOptionElement;
    this.cleanData();
    this.studyIndex = +studyHTMLIndex.value;
    this.getDiffExpData(this.studyIndex);
  }

  async getDiffExpData(index: number) {
    const api = new ApiUtils('files');
    const data = await api.getTopDiffExpData(this.user, this._selectedAnalysis + '/difExpTop' + (index + 1) + '.txt');
    const parsedData = JSON.parse(data);
    this.tableData = parsedData.data;
    this.createTable(parsedData.data, index);
  }

  createTable(data, index) {
    this.cleanData();
    let newTable = '<div name="tableDiffExpression" class="s10 pull-s1 push-s1">' +
    '<p><h5 class="center" style="color: #607d8b"> <b>Study ' + this.stdNames[index].toLocaleUpperCase() + '</b></h5></p>' +
    '<p><h6 class="center" style="color: #939393"> Showing Top 20 results. Download table for full results</h6></p><br>' +
    '<table class="responsive-table highlight"><thead>\n' +
      '    <tr>\n' +
      '      <th class="center">Entrez ID</th>\n' +
      '      <th class="">Gene Name</th>\n' +
      '      <th id="LORcol" class="center metaPointer">logFC</th>\n' +
      '      <th class="center">Statistic</th>\n' +
      '      <th id="pValCol" class="center metaPointer">P value</th>\n' +
      '      <th id="adjPvalCol" class="center metaPointer">Adjusted P value</th>\n' +
      '    </tr>\n' +
      '</thead>\n' +
      '<tbody id="difExpTable">';
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      newTable += '<tr id="difExpRow' + i + '">\n' +
        '      <td class="center">' +
        '<a href = https://www.ncbi.nlm.nih.gov/gene/?term=' + data[i].Entrez + ' target="_blank" </a>' +
        data[i].Entrez +
        '</td>' +
        '<td class="">' + data[i].GeneName + '</td>\n' +
        '      <td class="center">' + data[i].logFc.toFixed(3) + '</td>\n' +
        '      <td class="center">' + data[i].t.toFixed(3) + '</td>\n' +
        '      <td class="center">' + data[i].Pvalue.toExponential(4) + '</td>\n' +
        '      <td class="center">' + data[i].adjPvalue.toExponential(4) + '</td>\n' +
        '</tr>';
    }
    newTable += '</tbody></table></div>';
    document.getElementById('difExpTables').innerHTML += newTable;
    document.getElementById('LORcol').addEventListener('click', (e: Event) => this.orderByLORUP());
    document.getElementById('pValCol').addEventListener('click', (e: Event) => this.orderByPvalueUP());
    document.getElementById('adjPvalCol').addEventListener('click', (e: Event) => this.orderByPvalueUP());
  }

  cleanData() {
    const tableDiffExpression = document.getElementById('difExpTables');
    tableDiffExpression.innerHTML = '';
  }
  async downloadDifExp() {
    const api = new ApiUtils('files');
    const file = await api.downloadJobResult(this.user, this.selectedAnalysis + '/difExpTop' + (this.studyIndex + 1) + '.txt');
    const downFile = new File([file], this.stdNames[this.studyIndex] + 'DifExpression.csv',
      {type: 'text/plain;charset=utf-8'});
    saveAs(downFile);
  }

  checkSearchValDif() {
    const a = document.getElementById('search-inputDIF') as HTMLInputElement;
    let index = 0;
    for (const value of this.tableData) {
      if ((value.Entrez.includes(a.value) && a.value !== '') || (value.GeneName.includes(a.value) && a.value !== '')) {
        document.getElementById('difExpRow' + index).setAttribute('style', 'background-color: wheat;');
      } else {
        document.getElementById('difExpRow' + index).setAttribute('style', 'background-color: white');
      }
      index++;
    }
  }

  orderByLORUP() {
    if (DiffExpresionComponent.orderedbyLor === -1) {
      this.orderByLORDOWN();
    } else {
      DiffExpresionComponent.orderedbyLor = -1;
      this.tableData.sort((a , b) => {
        if (a.logFc > b.logFc) {
          return 1;
        }
        if (b.logFc > a.logFc) {
          return -1;
        }
        return 0;
      });
    }
    this.createTable(this.tableData, this.studyIndex);

  }
  orderByLORDOWN() {
    DiffExpresionComponent.orderedbyLor = 1;
    this.tableData.sort((a , b) => {
      if (a.logFc > b.logFc) {
        return -1;
      }
      if (b.logFc > a.logFc) {
        return 1;
      }
      return 0;
    });

    this.createTable(this.tableData, this.studyIndex);
  }

  orderByPvalueUP() {
    if (DiffExpresionComponent.orderedbypVal === -1) {
      this.orderByPvalueDOWN();
    } else {
      DiffExpresionComponent.orderedbypVal = -1;
      this.tableData.sort((a, b) => {
        if (a.Pvalue > b.Pvalue) {
          return 1;
        }
        if (b.Pvalue > a.Pvalue) {
          return -1;
        }
        return 0;
      });
    }
    this.createTable(this.tableData, this.studyIndex);
  }

  orderByPvalueDOWN() {
    DiffExpresionComponent.orderedbypVal = 1;
    this.tableData.sort((a , b) => {
      if (a.Pvalue > b.Pvalue) {
        return -1;
      }
      if (b.Pvalue > a.Pvalue) {
        return 1;
      }
      return 0;
    });
    this.createTable(this.tableData, this.studyIndex);
  }

  orderByadjPvalueUP() {
    if (DiffExpresionComponent.orderedbyadj === -1) {
      this.orderByadjPvalueDOWN();
    } else {
      DiffExpresionComponent.orderedbyadj = -1;
      this.tableData.sort((a, b) => {
        if (a.adjPvalue > b.adjPvalue) {
          return 1;
        }
        if (b.adjPvalue > a.adjPvalue) {
          return -1;
        }
        return 0;
      });
    }
    this.createTable(this.tableData, this.studyIndex);
  }
  orderByadjPvalueDOWN() {
    DiffExpresionComponent.orderedbyadj = 1;
    this.tableData.sort((a , b) => {
      if (a.adjPvalue > b.adjPvalue) {
        return -1;
      }
      if (b.adjPvalue > a.adjPvalue) {
        return 1;
      }
      return 0;
    });
    this.createTable(this.tableData, this.studyIndex);
  }

  checkSearchPValDif() {
    const pvaldifsearch = document.getElementById('filter-inputPDIF') as HTMLInputElement;
    let index = 0;
    for (const value of this.tableData) {
      if ((parseFloat(value.adjPvalue) < parseFloat(pvaldifsearch.value)) && pvaldifsearch.value !== '') {
        document.getElementById('difExpRow' + index).setAttribute('style', 'background-color: wheat;');
      } else {
        document.getElementById('difExpRow' + index).setAttribute('style', 'background-color: white');
      }
      index++;
    }
  }

}
