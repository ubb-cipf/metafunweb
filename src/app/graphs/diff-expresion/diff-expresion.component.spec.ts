import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiffExpresionComponent } from './diff-expresion.component';

describe('DiffExpresionComponent', () => {
  let component: DiffExpresionComponent;
  let fixture: ComponentFixture<DiffExpresionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiffExpresionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiffExpresionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
