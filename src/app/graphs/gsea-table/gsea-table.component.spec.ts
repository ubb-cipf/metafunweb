import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GseaTableComponent } from './gsea-table.component';

describe('GseaTableComponent', () => {
  let component: GseaTableComponent;
  let fixture: ComponentFixture<GseaTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GseaTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GseaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
