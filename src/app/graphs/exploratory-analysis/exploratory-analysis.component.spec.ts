import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploratoryAnalysisComponent } from './exploratory-analysis.component';

describe('ExploratoryAnalysisComponent', () => {
  let component: ExploratoryAnalysisComponent;
  let fixture: ComponentFixture<ExploratoryAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploratoryAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploratoryAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
