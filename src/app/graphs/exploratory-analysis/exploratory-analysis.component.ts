import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {NgxSmartLoaderService} from 'ngx-smart-loader';

@Component({
  selector: 'app-exploratory-analysis',
  templateUrl: './exploratory-analysis.component.html',
  styleUrls: ['./exploratory-analysis.component.css']
})
export class ExploratoryAnalysisComponent implements OnInit, OnChanges {
  stdNames;
  user;
  @Input() selectedAnalysis;
  @Input() loadData;
  private _selectedAnalysis;
  stdCount = 0;
  constructor(private cookieService: CookieService, private loader: NgxSmartLoaderService) { }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    this.loader.start('jobsLoader');
  }

  async ngOnChanges(changes: SimpleChanges) {
      if (this.loadData === 1) {
          let count = 0;
          // const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
          // this._selectedAnalysis = selectedAnalysis.currentValue;
          // const loadData: SimpleChange = changes.loadData;
          this.stdNames = JSON.parse(sessionStorage.getItem('RESULTS_STDNAMES'));
          this._selectedAnalysis = sessionStorage.getItem('SELECTED_ANALYSIS');
          const api = new ApiUtils('files');
          count = await api.getPlotCount(this.user, this.selectedAnalysis);
          this.stdCount = +count;
      }
  }

  // loadDataModule(index: any) {
  //   this.cleanData()
  //   this.createGraphics(index)
  //   const jobName = this.stdNames[index].name;
  //   this.selectedAnalysis = jobName;
  //   for (let i = 0 ; i < this.stdNames.length ; i++) {
  //     if (this.stdNames[i].name === this.selectedAnalysis) {
  //       document.getElementById('graphsdiv').setAttribute('class', '');
  //       document.getElementById('label' + index).setAttribute('class', 'metaPointer active');
  //     }else{
  //       document.getElementById('graphsdiv').setAttribute('class', 'hide');
  //       document.getElementById('label' + index).setAttribute('class', 'metaPointer');
  //     }
  //   }
  // }

  // createGraphics(index: number){
  //   let newTable = '<div id="graphsdiv" class="hide">\n' +
    // tslint:disable-next-line:max-line-length
  //    '  <app-scatter-plot [selectedAnalysis]=' + this.selectedAnalysis + ' [stdID]=' + index + ' [loadData]=' + this.loadData + '></app-scatter-plot>\n' +
  //    '  <app-scatter-plot3d [selectedAnalysis]=' + this.selectedAnalysis + ' [stdID]=' + index + ' [loadData]=' + this.loadData + '></app-scatter-plot3d>\n' +
  //    '  <app-box-plot [selectedAnalysis]=' + this.selectedAnalysis + ' [stdID]=' + index + ' [loadData]=' + this.loadData + '></app-box-plot>\n' + 
  //    '  <app-clusterplot [selectedAnalysis]=' + this.selectedAnalysis + ' [stdID]=' + index + ' [loadData]=' + this.loadData + '></app-clusterplot>\n' + 
  //   '<br><br><br></div>'
    
  //   document.getElementById('graphsdiv').innerHTML += newTable;

    
  // }

  // cleanData() {
  //   const graphsdiv = document.getElementById('graphsdiv');
  //   graphsdiv.innerHTML = '';
  // }

  loadDataModule(index: any) {
    if (this.loadData !== 1) {
        this.loadData = 1;
    }
    const state = document.getElementById('graphsdiv' + index).getAttribute('class');
    if (state === '') {
        document.getElementById('graphsdiv' + index).setAttribute('class', 'hide');
        document.getElementById('open' + index).setAttribute('class', 'btn waves-effect right');
        document.getElementById('close' + index).setAttribute('class', 'btn waves-effect right hide');
    } else {
        document.getElementById('graphsdiv' + index).setAttribute('class', '');
        document.getElementById('open' + index).setAttribute('class', 'btn waves-effect right hide');
        document.getElementById('close' + index).setAttribute('class', 'btn waves-effect right');
    }
  }
}
