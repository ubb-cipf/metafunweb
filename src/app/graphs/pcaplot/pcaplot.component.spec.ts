import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcaplotComponent } from './pcaplot.component';

describe('PcaplotComponent', () => {
  let component: PcaplotComponent;
  let fixture: ComponentFixture<PcaplotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcaplotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcaplotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
