import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-documentacion',
  templateUrl: './documentacion.component.html',
  styleUrls: ['./documentacion.component.css']
})
export class DocumentacionComponent implements OnInit, OnChanges {
  private static orderedbyLor = 1;
  private static orderedbypVal = 1;
  private static orderedbyadj = 1;
  private checkboxIsChecked = true;
  private parsedData;
  public goData;
  public links = [];
  private user;
  design: any;
  private  _selectedAnalysis;
  @Input() selectedAnalysis;

  constructor(private cookieService: CookieService) { }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    document.getElementById('lorCol').addEventListener('click', (e: Event) => this.orderByLORUP());
    document.getElementById('pvalCol').addEventListener('click', (e: Event) => this.orderByPvalueUP());
    document.getElementById('adjPvalCol').addEventListener('click', (e: Event) => this.orderByPvalueUP());
  }

  async ngOnChanges(changes: SimpleChanges) {
    this.links = [];
    this.user = this.cookieService.get('userLoged');
    // const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    // this._selectedAnalysis = selectedAnalysis.currentValue;
    this._selectedAnalysis = sessionStorage.getItem('SELECTED_ANALYSIS');
    const api = new ApiUtils('files');
    if (this._selectedAnalysis !== '') {
      console.log (this._selectedAnalysis)
      let data = null
      if (this.checkboxIsChecked) {
        this.checkboxIsChecked = true
        data = await api.getPartialMetaTableData(this.user, this._selectedAnalysis);
      }else {
        this.checkboxIsChecked = false
        data = await api.getMetaTableData(this.user, this._selectedAnalysis);

      }
      // this.design = await api.getJobDesign(this.user, this._selectedAnalysis);
      this.design = sessionStorage.getItem('RESULTS_CONTRAST');
      if (this._selectedAnalysis !== 'undefined' && this._selectedAnalysis !== undefined) {
        console.log (data)
        this.parsedData = JSON.parse(data);
        for (let i = 0 ; i < this.parsedData.data.length ; i++) {
          if (this.parsedData.data[i].GOTerm.match(new RegExp('GO:*'))) {
              this.links.push('https://www.ebi.ac.uk/QuickGO/term/' + this.parsedData.data[i].GOTerm);
            } else if (this.parsedData.data[i].GOTerm.match(new RegExp('P-*'))) {
              this.links.push('https://www.genome.jp/kegg-bin/show_pathway?select_scale=1.0&query=' +
                                         this.parsedData.data[i].goName.split(':')[1].split(' ')[1] +
                '&map=' + this.parsedData.data[i].GOTerm.split('-')[1] + '&scale=&orgs=&auto_image=&show_description=hide&mult i_query=');
              }
          }
        this.goData = this.parsedData.data;
      }
    }
  }

  async checkBoxChange() {
    this.checkboxIsChecked = !this.checkboxIsChecked
    this.links = [];
    this.user = this.cookieService.get('userLoged');
    // const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    // this._selectedAnalysis = selectedAnalysis.currentValue;
    this._selectedAnalysis = sessionStorage.getItem('SELECTED_ANALYSIS');
    const api = new ApiUtils('files');
    if (this._selectedAnalysis !== '') {
      let data = null
      if (this.checkboxIsChecked) {
        data = await api.getPartialMetaTableData(this.user, this._selectedAnalysis);        
      }else { 
        data = await api.getMetaTableData(this.user, this._selectedAnalysis);
      }
      // this.design = await api.getJobDesign(this.user, this._selectedAnalysis);
      this.design = sessionStorage.getItem('RESULTS_CONTRAST');
      if (this._selectedAnalysis !== 'undefined' && this._selectedAnalysis !== undefined) {
        this.parsedData = JSON.parse(data);
        for (let i = 0 ; i < this.parsedData.data.length ; i++) {
          if (this.parsedData.data[i].GOTerm.match(new RegExp('GO:*'))) {
              this.links.push('https://www.ebi.ac.uk/QuickGO/term/' + this.parsedData.data[i].GOTerm);
            } else if (this.parsedData.data[i].GOTerm.match(new RegExp('P-*'))) {
              this.links.push('https://www.genome.jp/kegg-bin/show_pathway?select_scale=1.0&query=' +
                                         this.parsedData.data[i].goName.split(':')[1].split(' ')[1] +
                '&map=' + this.parsedData.data[i].GOTerm.split('-')[1] + '&scale=&orgs=&auto_image=&show_description=hide&mult i_query=');
              }
          }
        this.goData = this.parsedData.data;
      }
    }
  }
  showGraphs(index) {
    document.getElementById('forestplot' + index).setAttribute('class', 'row');
    document.getElementById('infoshow' + index).setAttribute('class', 'row hide');
    document.getElementById('infohide' + index).setAttribute('class', 'row');
  }

  hideGraphs(index) {
    document.getElementById('forestplot' + index).setAttribute('class', 'row hide');
    document.getElementById('infoshow' + index).setAttribute('class', 'row');
    document.getElementById('infohide' + index).setAttribute('class', 'row hide');
  }

  orderByLORUP() {
    if (DocumentacionComponent.orderedbyLor === -1) {
      this.orderByLORDOWN();
    } else {
      DocumentacionComponent.orderedbyLor = -1;
      this.goData.sort((a , b) => {
        if (a.lor > b.lor) {
          return 1;
        }
        if (b.lor > a.lor) {
          return -1;
        }
        return 0;
      });
    }
  }
  orderByLORDOWN() {
    DocumentacionComponent.orderedbyLor = 1;
    this.goData.sort((a , b) => {
      if (a.lor > b.lor) {
        return -1;
      }
      if (b.lor > a.lor) {
        return 1;
      }
      return 0;
    });
  }
  orderByPvalueUP() {
    if (DocumentacionComponent.orderedbypVal === -1) {
      this.orderByPvalueDOWN();
    } else {
      DocumentacionComponent.orderedbypVal = -1;
      this.goData.sort((a, b) => {
        if (a.pval > b.pval) {
          return 1;
        }
        if (b.pval > a.pval) {
          return -1;
        }
        return 0;
      });
    }
  }
  orderByPvalueDOWN() {
    DocumentacionComponent.orderedbypVal = 1;
    this.goData.sort((a , b) => {
      if (a.pval > b.pval) {
        return -1;
      }
      if (b.pval > a.pval) {
        return 1;
      }
      return 0;
    });
  }
  orderByadjPvalueUP() {
    if (DocumentacionComponent.orderedbyadj === -1) {
      this.orderByadjPvalueDOWN();
    } else {
      DocumentacionComponent.orderedbyadj = -1;
      this.goData.sort((a, b) => {
        if (a.adjPval > b.adjPval) {
          return 1;
        }
        if (b.adjPval > a.adjPval) {
          return -1;
        }
        return 0;
      });
    }
  }
  orderByadjPvalueDOWN() {
    DocumentacionComponent.orderedbyadj = 1;
    this.goData.sort((a , b) => {
      if (a.adjPval > b.adjPval) {
        return -1;
      }
      if (b.adjPval > a.adjPval) {
        return 1;
      }
      return 0;
    });
  }

}
