import { Component, OnInit } from '@angular/core';
import ApiUtils from '../utils/ApiUtils';
import * as CryptoJS from 'crypto-js';
import {MenuComponent} from '../menu/menu.component';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  userLoged: boolean = false;

  constructor(private cookieService: CookieService, private router: Router) { }

  ngOnInit() {
    if (this.cookieService.check('userLoged')) {
      this.router.navigate(['metafun/userprofile']);
      this.userLoged = true
    }
  }

  async login_anon() {
    const api = new ApiUtils('users');
    const apiFiles = new ApiUtils('files');
    const mail = '' + Date.now();
    const pass = CryptoJS.MD5('' + Date.now() + '.*fa');
    const name = 'anon' + mail;
    let token = ''
    if (this.userLoged === false) {
      const res = await api.signUpRequest(name, mail, pass);
      apiFiles.createFolder(name, 'files/MyFolder', 'files')
      token = await api.getUserToken(mail)
      this.cookieService.set('userToken', token)
    }
    // @ts-ignore
    if (res === true) {
      this.cookieService.set('userLoged', name, 2, '', '', true, 'Lax');
      MenuComponent.reloadSignUpText('');
      MenuComponent.reloadLogIntext(name);
      this.router.navigate(['metafun/newjob']);
    }
  }
}
