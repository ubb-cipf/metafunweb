import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import ApiUtils from '../utils/ApiUtils';
import {NgxSmartLoaderService} from 'ngx-smart-loader';

@Component({
  selector: 'app-jobs-page',
  templateUrl: './jobs-page.component.html',
  styleUrls: ['./jobs-page.component.css']
})
export class JobsPageComponent implements OnInit {
  user: string;
  files;
  isGSEA = true;
  private jobs = [];
  public selectedAnalysis: string;
  public analysisName: string;
  functionalAnalysis = 'Functional Profiling';
  loadExploratory = 0;
  loadDiffExp = 0;
  loadFunc = 0;
  loadMeta = 0;

  constructor(private cookieService: CookieService, private router: Router, public loader: NgxSmartLoaderService) { }

  ngOnInit() {
    this.loadExploratory = 0;
    this.loadDiffExp = 0;
    this.loadFunc = 0;
    this.loadMeta = 0;
    sessionStorage.setItem('RESULTS_CONTRAST', '');
    sessionStorage.setItem('RESULTS_STDNAMES', '');
    sessionStorage.setItem('SELECTED_ANALYSIS', '');
    this.user = this.cookieService.get('userLoged');
    if (this.user !== '') {
      this.loadUserJobs();
    } else {
      this.router.navigate(['metafun']);
    }
  }

  toNewJob() {
    this.router.navigate(['metafun/new-job']);
  }

  showSummary() {
    document.getElementById('summaryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2');
    document.getElementById('exploratoryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('differentialExpression')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('geneSetAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('metaAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('summaryli').setAttribute('class', 'metaPointer active');
    document.getElementById('exploratoryli').setAttribute('class', 'metaPointer');
    document.getElementById('diffexpli').setAttribute('class', 'metaPointer');
    document.getElementById('gseali').setAttribute('class', 'metaPointer');
    document.getElementById('metali').setAttribute('class', 'metaPointer');
  }

  showExploratory() {
    document.getElementById('summaryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('exploratoryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2');
    document.getElementById('differentialExpression')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('geneSetAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('metaAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('summaryli').setAttribute('class', 'metaPointer');
    document.getElementById('exploratoryli').setAttribute('class', 'metaPointer active');
    document.getElementById('diffexpli').setAttribute('class', 'metaPointer');
    document.getElementById('gseali').setAttribute('class', 'metaPointer');
    document.getElementById('metali').setAttribute('class', 'metaPointer');
    this.loadExploratory = 1;
  }

  showDiffExp() {
    document.getElementById('summaryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('exploratoryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('differentialExpression')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2');
    document.getElementById('geneSetAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('metaAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('summaryli').setAttribute('class', 'metaPointer');
    document.getElementById('exploratoryli').setAttribute('class', 'metaPointer');
    document.getElementById('diffexpli').setAttribute('class', 'metaPointer active');
    document.getElementById('gseali').setAttribute('class', 'metaPointer');
    document.getElementById('metali').setAttribute('class', 'metaPointer');
    this.loadDiffExp = 1;
  }

  showGSEA() {
    document.getElementById('summaryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('exploratoryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('differentialExpression')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('geneSetAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2');
    document.getElementById('metaAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('summaryli').setAttribute('class', 'metaPointer');
    document.getElementById('exploratoryli').setAttribute('class', 'metaPointer');
    document.getElementById('diffexpli').setAttribute('class', 'metaPointer');
    document.getElementById('gseali').setAttribute('class', 'metaPointer active');
    document.getElementById('metali').setAttribute('class', 'metaPointer');
    this.loadFunc = 1;
  }

  showMeta() {
    document.getElementById('summaryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('exploratoryAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('differentialExpression')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('geneSetAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2 hide');
    document.getElementById('metaAnalysis')
      .setAttribute('class', 'scrollableDiv row col s10 push-s2');
    document.getElementById('summaryli').setAttribute('class', 'metaPointer');
    document.getElementById('exploratoryli').setAttribute('class', 'metaPointer');
    document.getElementById('diffexpli').setAttribute('class', 'metaPointer');
    document.getElementById('gseali').setAttribute('class', 'metaPointer ');
    document.getElementById('metali').setAttribute('class', 'metaPointer active');
    this.loadMeta = 1;
  }


  async loadUserJobs() {
    const api = new ApiUtils('jobs');
    const resp = await api.getJobsFromUser(this.user);
    this.files  = JSON.parse(resp);
  }

  jobShowResults(jobIndex: number) {
    this.loadExploratory = 0;
    this.loadDiffExp = 0;
    this.loadFunc = 0;
    this.loadMeta = 0;
    const jobName = this.files[jobIndex].name;
    this.selectedAnalysis = jobName;
    if (this.selectedAnalysis.length > 25) {
      this.analysisName = this.selectedAnalysis.substring(0,20) + '...'
    } else {
      this.analysisName = this.selectedAnalysis
    }

    for (let i = 0 ; i < this.files.length ; i++) {
      document.getElementById('job' + i).setAttribute('style', '');
      if (this.files[i].name === this.selectedAnalysis) {
        document.getElementById('job' + i).setAttribute('style', 'background-color:#F8E7C6; border-left: 10px solid #E38930; color: #BC7836');
        if (this.files[i].functional === '-GSA') {
          this.isGSEA = true;
          this.functionalAnalysis = 'GSA';
        } else {
          this.isGSEA = false;
          this.functionalAnalysis = 'Hipathia';
        }
      } else {
      }
    }
    document.getElementById('selectAnalysisprompt').setAttribute('class','hide')
    document.getElementById('analysisResultsDiv').setAttribute('class', '');
    this.showSummary();
  }

  jobShowButton(index) {
  }

  jobDeleteButton(index) {
    alert('Are you sure you want to delete ' + this.files[index].name + '?');
  }

  async jobDownloadButton(index) {
    const api = new ApiUtils('jobs');
    const res = await api.downloadJobResult(this.user, this.files[index].name);
    const blob = new Blob([res], {type: 'application/pdf'});
    // FileSaver.saveAs(blob, this.files[index].name + '.pdf');
  }

  cleanTable() {
    const table = document.getElementById('jobsTable') as HTMLTableElement;
    table.innerHTML = '';
  }

  hideShow(hideDiv) {
    const exploratoryDiv = document.getElementById('exploratoryDiv');
    const expressionDiv = document.getElementById('difExpressionTable');
    const gesaDiv = document.getElementById('GSEADivTable');
    switch (hideDiv) {
      case 0:
        if (exploratoryDiv.getAttribute('class').match('show')) {
          exploratoryDiv.setAttribute('class', 'hide');
        } else {
          exploratoryDiv.setAttribute('class', 'show');
        }
        break;
      case 1:
        if (expressionDiv.getAttribute('class').match('show')) {
          expressionDiv.setAttribute('class', 'hide');
        } else {
          expressionDiv.setAttribute('class', 'show');
        }
        break;
      case 2:
        if (gesaDiv.getAttribute('class').match('')) {
          gesaDiv.setAttribute('class', 'hide');
        } else {
          gesaDiv.setAttribute('class', 'show');
        }
        break;
      default:
        break;
    }
  }
}
