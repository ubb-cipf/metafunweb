import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxSmartModalModule } from 'ngx-smart-modal';

import { AppComponent } from './app.component';
import { DocumentacionComponent } from './documentacion/documentacion.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { SignupComponent } from './signup/signup.component';
import { DataPageComponent } from './data-page/data-page.component';
import { JobsPageComponent } from './jobs-page/jobs-page.component';
import { NewJobComponent } from './new-job/new-job.component';
import { BoxplotComponent } from './graphs/boxplot/boxplot.component';
import { ClusterplotComponent } from './graphs/clusterplot/clusterplot.component';
import { DiffExpresionComponent } from './graphs/diff-expresion/diff-expresion.component';
import { GseaTableComponent } from './graphs/gsea-table/gsea-table.component';
import { MetaTableComponent } from './graphs/meta-table/meta-table.component';
import { PcaplotComponent } from './graphs/pcaplot/pcaplot.component';
import { HipathiaTableComponent } from './graphs/hipathia-table/hipathia-table.component';
import { FileBrowserComponent } from './file-browser/file-browser.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import {RouterModule} from '@angular/router';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import {PlotlyModule} from 'angular-plotly.js';
import {CookieService} from 'ngx-cookie-service';
import { UserprofileComponent } from './userprofile/userprofile.component';
import {APP_ROUTES} from './app.routing';
import { ForestPlotComponent } from './graphs/forest-plot/forest-plot.component';
import { FunnelPlotComponent } from './graphs/funnel-plot/funnel-plot.component';
import { ExploratoryAnalysisComponent } from './graphs/exploratory-analysis/exploratory-analysis.component';
import { BoxPlotComponent } from './newPlots/box-plot/box-plot.component';
import { ScatterPlotComponent } from './newPlots/scatter-plot/scatter-plot.component';
import {NgxSmartLoaderModule} from 'ngx-smart-loader';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
import { FileManagerComponent } from './file-manager/file-manager.component';
import { SummaryComponent } from './graphs/summary/summary.component';
import {FileUploadModule} from 'ng2-file-upload';
import { HelpComponent } from './help/help.component';
import { ScatterPlot3dComponent } from './newPlots/scatter-plot3d/scatter-plot3d.component';
PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    AppComponent,
    DocumentacionComponent,
    FooterComponent,
    MenuComponent,
    SignupComponent,
    DataPageComponent,
    JobsPageComponent,
    NewJobComponent,
    BoxplotComponent,
    ClusterplotComponent,
    DiffExpresionComponent,
    GseaTableComponent,
    MetaTableComponent,
    PcaplotComponent,
    HipathiaTableComponent,
    FileBrowserComponent,
    HomeComponent,
    LoginComponent,
    UserprofileComponent,
    ForestPlotComponent,
    FunnelPlotComponent,
    ExploratoryAnalysisComponent,
    BoxPlotComponent,
    ScatterPlotComponent,
    FileUploaderComponent,
    FileManagerComponent,
    SummaryComponent,
    HelpComponent,
    ScatterPlot3dComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    DragDropModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    PlotlyModule,
    DragDropModule,
    NgxSmartModalModule.forRoot(),
    NgxSmartLoaderModule.forRoot(),
    FileUploadModule,
    APP_ROUTES
  ],
  providers: [CookieService, NgxSmartLoaderModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
