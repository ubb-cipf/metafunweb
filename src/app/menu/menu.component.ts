import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  constructor(private cookieService: CookieService, private router: Router) {  }
  public loggedUser: Boolean;
  public static reloadLogIntext(text) {
    document.getElementById('logInButton').innerText = text;
    if (text != '') {
      document.getElementById('logoutButton').hidden = false;
    }

  }
  public  static reloadSignUpText(text) {
    document.getElementById('signUpButton').innerText = text;
  }

  ngOnInit() {
    const user = this.cookieService.get('userLoged');
    if (user !== '' || user == null) {
        if (user.substring(0, 4).match('anon')) {
          document.getElementById('logInButton').innerText = 'Anonymous';
        } else {
          document.getElementById('logInButton').innerText = user;
        }
        document.getElementById('signUpButton').innerText = '';
        document.getElementById('logoutButton').hidden = false;
    } else {
      document.getElementById('logInButton').innerText = 'LogIn';
      document.getElementById('signUpButton').innerText = 'SignUp';
      document.getElementById('logoutButton').hidden = true;
    }
  }

  logOut() {
    this.cookieService.delete('userLoged');
    this.router.navigate(['metafun']);
    MenuComponent.reloadLogIntext('LogIn');
    MenuComponent.reloadSignUpText('SignUp');
    document.getElementById('logoutButton').hidden = true;
  }

}
