import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import ApiUtils from '../../utils/ApiUtils';

@Component({
  selector: 'app-scatter-plot3d',
  templateUrl: './scatter-plot3d.component.html',
  styleUrls: ['./scatter-plot3d.component.css']
})
export class ScatterPlot3dComponent implements OnInit, OnChanges {
  private static loadingCount = 0;
  public plot = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  private user;
  public debug = false;
  public useResizeHandler = true;
  private contrast = '';
  private bpValues = [];
  private stdNames = null;
  @Input() stdID: string;
  @Input() study: string;
  @Input() selectedAnalysis: string;
  @Input() loadData: number;
  private _selectedAnalysis;

  constructor(
    private cookieService: CookieService,
  ) {}

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');

  }

  async ngOnChanges() {
    if (this.loadData === 1) {
        const pcaPlot = document.getElementById('plotScatter');
        const api = new ApiUtils('jobs');
        this.contrast = sessionStorage.getItem('RESULTS_CONTRAST'); // await api.getJobDesign(this.user, this.selectedAnalysis);
        await this.getPcaPlotValues();
    }
  }

  async getPcaPlotValues() {
    const caseTrace = {x: [], y: [], z: [], text: [], name: 'Case', type: 'scatter3d', mode: 'markers', showlegend: true,
      marker: {size: 2, color: '#FFA500'}};
    const controlTrace = {x: [], y: [], z: [], text: [], name: 'Control', type: 'scatter3d', mode: 'markers',
      showlegend: true, marker: {size: 2, color: '#009E73'}};
    const METrace = {x: [], y: [], z: [], text: [], name: 'Case Female', type: 'scatter3d', mode: 'markers',
      showlegend: true, marker: {size: 2, color: '#FFA500'}};
    const MSTrace = {x: [], y: [], z: [], text: [], name: 'Control Female', type: 'scatter3d', mode: 'markers',
      showlegend: true, marker: {size: 2, color: '#009E73'}};
    const HETrace = {x: [], y: [], z: [], text: [], name: 'Case Male', type: 'scatter3d', mode: 'markers',
      showlegend: true, marker: {size: 2, color: '#808080'}};
    const HSTrace = {x: [], y: [], z: [], text: [], name: 'Control Male', type: 'scatter3d', mode: 'markers',
      showlegend: true, marker: {size: 2, color: '#56B4E9'}};

    const emptyPlot = {
      data: [],
      layout: {
        title: 'PCA Plot (3D)',
        showlegend: true,
        scene: {
          xaxis: {
            title: 'PC 1'
          },
          yaxis: {
            title: 'PC 2'
          },
          zaxis: {
            title: 'PC 3'
          }
        }
      }
    };

    if (this.contrast.match('Case-Control')) {
      emptyPlot.data.push(controlTrace, caseTrace);
    } else {
      emptyPlot.data.push(METrace, MSTrace, HETrace, HSTrace);
    }
    const api = new ApiUtils('files');
    const apiJ = new ApiUtils('jobs');
    this.contrast = await apiJ.getJobDesign(this.user, this.selectedAnalysis);
    // const aux = await api.getStudiesNames(this.user, this.selectedAnalysis);
    const rawData = await api.getPcaplotData(this.user, this.selectedAnalysis + '/pca' + (this.stdID + 1) + '.txt');
    const parsedData = JSON.parse(rawData);
    const test = parsedData.data;
    // this.stdNames = JSON.parse(sessionStorage.getItem('RESULTS_STDNAMES')); // aux.split('\n');

    for (let i = 0; i < parsedData.data.length; i++) {
      if (test[i].design.match('Case')) {
        caseTrace.x.push(test[i].PC1);
        caseTrace.y.push(test[i].PC2);
        caseTrace.z.push(test[i].PC3);
        caseTrace.text.push(test[i].sample);
      } else if (test[i].design.match('Control')) {
        controlTrace.x.push(test[i].PC1);
        controlTrace.y.push(test[i].PC2);
        controlTrace.z.push(test[i].PC3);
        controlTrace.text.push(test[i].sample);
      } else if (test[i].design.match('ME')) {
        METrace.x.push(test[i].PC1);
        METrace.y.push(test[i].PC2);
        METrace.z.push(test[i].PC3);
        METrace.text.push(test[i].sample);
      } else if (test[i].design.match('MS')) {
        MSTrace.x.push(test[i].PC1);
        MSTrace.y.push(test[i].PC2);
        MSTrace.z.push(test[i].PC3);
        MSTrace.text.push(test[i].sample);
      } else if (test[i].design.match('HE')) {
        HETrace.x.push(test[i].PC1);
        HETrace.y.push(test[i].PC2);
        HETrace.z.push(test[i].PC3);
        HETrace.text.push(test[i].sample);
      } else if (test[i].design.match('HS')) {
        HSTrace.x.push(test[i].PC1);
        HSTrace.y.push(test[i].PC2);
        HSTrace.z.push(test[i].PC3);
        HSTrace.text.push(test[i].sample);
      }
    }
    this.plot = emptyPlot;
  }
}
