import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import ApiUtils from '../../utils/ApiUtils';
import {NgxSmartLoaderService} from 'ngx-smart-loader';

@Component({
  selector: 'app-box-plot',
  templateUrl: './box-plot.component.html',
  styleUrls: ['./box-plot.component.css']
})
export class BoxPlotComponent implements OnInit, OnChanges {
  private user;
  public debug = false;
  public useResizeHandler = true;
  private contrast = '';
  private bpValues = [];
  private stdNames = null;
  @Input() stdID: string;
  @Input() study: string;
  @Input() selectedAnalysis: string;
  @Input() loadData: number;

  public boxPlot = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
  };

  constructor(
    private cookieService: CookieService,
    public loader: NgxSmartLoaderService
  ) {}

  async ngOnInit() {
    this.loader.start('myLoader');
  }

  async ngOnChanges() {
    if (this.loadData === 1) {
      this.user = this.cookieService.get('userLoged');
      const emptyboxPlot = {
        data: [

        ],
        layout: {
          title: 'Box Plot by Samples'
        },
        config: {
          staticPlot: false,
          responsive: true
        },
      };

      const leg1 = {x: [], y: [], name: 'Case', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip',
        marker: {color: '#FFA500'}};
      const leg2 = {x: [], y: [], name: 'Control', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip',
        marker: {color: '#009E73'}};
      const legME = {x: [], y: [], name: 'Case Female', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip',
        marker: {color: '#FFA500'}};
      const legMS = {x: [], y: [], name: 'Control Female', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip',
        marker: {color: '#009E73'}};
      const legHE = {x: [], y: [], name: 'Case Male', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip',
        marker: {color: '#808080'}};
      const legHS = {x: [], y: [], name: 'Control Male', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip',
        marker: {color: '#56B4E9'}};
      const api = new ApiUtils('files');
      const apiJ = new ApiUtils('jobs');
      const rawData = await api.getBoxplotData(this.user, this.selectedAnalysis + '/boxplot' + (+this.stdID + 1)  + '.txt');
      this.contrast = sessionStorage.getItem('RESULTS_CONTRAST');// await apiJ.getJobDesign(this.user, this.selectedAnalysis);
      const parsedData = JSON.parse(rawData);
      const test = parsedData.data;
      let bpDataValues;
      test.sort((a, b) => {
        return b.design < a.design ?  1
          : b.design > a.design ? -1
            : 0;
      });

      for (let i = 0; i < parsedData.data.length; i++) {
        bpDataValues = this.boxplotValues(test[i].values);
        let colorMarker: any;
        let legGroup: any;
        switch (test[i].design) {
          case 'Control':
            colorMarker = {color: '#009E73'};
            legGroup = 'Control';
            break;
          case 'Case':
            colorMarker = {color: '#FFA500'};
            legGroup = 'Case';
            break;
          case 'ME':
            colorMarker = {color: '#FFA500'};
            legGroup = 'Case Female';
            break;
          case 'MS':
            colorMarker = {color: '#009E73'};
            legGroup = 'Control Female';
            break;
          case 'HE':
            colorMarker = {color: '#808080'};
            legGroup = 'Case Male';
            break;
          case 'HS':
            colorMarker = {color: '#56B4E9'};
            legGroup = 'Control Male';
            break;
        }
        emptyboxPlot.data.push({
          y: bpDataValues[0],
          outliers: bpDataValues[1],
          showlegend: false,
          name: test[i].sample,
          marker: colorMarker,
          text: test[i].design,
          legendgroup: legGroup,
          type: 'box'});
        if (test[i].design.match('Case')) {
          leg1.y.push(bpDataValues[0][3]);
          leg1.x.push(test[i].sample);
        } else if (test[i].design.match('Control')) {
          leg2.y.push(bpDataValues[0][3]);
          leg2.x.push(test[i].sample);
        } else if (test[i].design.match('ME')) {
          legME.y.push(bpDataValues[0][3]);
          legME.x.push(test[i].sample);
        } else if (test[i].design.match('MS')) {
          legMS.y.push(bpDataValues[0][3]);
          legMS.x.push(test[i].sample);
        } else if (test[i].design.match('HE')) {
          legHE.y.push(bpDataValues[0][3]);
          legHE.x.push(test[i].sample);
        } else if (test[i].design.match('HS')) {
          legHS.y.push(bpDataValues[0][3]);
          legHS.x.push(test[i].sample);
        }
      }
      if (this.contrast.match('Case-Control')) {
        emptyboxPlot.data.push(leg1, leg2);
      } else {
        emptyboxPlot.data.push(legME, legMS, legHE, legHS);
      }
      this.boxPlot = emptyboxPlot;
      // this.loader.stop('myLoader');
    }
  }

  boxplotValues(jsonbpData) {
    const outliers = [];
    const Q1 = jsonbpData.Q1;
    const Q2 = jsonbpData.Q2;
    const Q3 = jsonbpData.Q3;
    let wMin = +Q1 - (1.5 * (+Q3 - +Q1));
    let wMax = +Q3 + (1.5 * (+Q3 - +Q1));
    if (wMin > jsonbpData.min) {
      outliers.push(jsonbpData.min);
    } else {
      wMin = jsonbpData.min;
    }

    if (wMax < jsonbpData.max) {
      outliers.push(jsonbpData.max);
    } else {
      wMax = jsonbpData.max;
    }
    const yData = [wMin, Q1, Q1, Q2, Q2, Q3, Q3, wMax];

    return [yData, outliers];
  }
}
