import {HomeComponent} from './home/home.component';
import {RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {NewJobComponent} from './new-job/new-job.component';
import {UserprofileComponent} from './userprofile/userprofile.component';
import {JobsPageComponent} from './jobs-page/jobs-page.component';


const appRoutes = [

  { path: 'metafun', component: HomeComponent, data: {page: '1'}},
  { path: 'metafun/login', component: LoginComponent, data: {page: '2'}},
  { path: 'metafun/signup', component: SignupComponent, data: {page: '3'}},
  { path: 'metafun/newjob', component: NewJobComponent, data: {page: '4'}},
  { path: 'metafun/home', component: HomeComponent, data: {page: '5'}},
  { path: 'metafun/userprofile', component: UserprofileComponent, data: {page: '6'}},
  { path: 'metafun/jobspage', component: JobsPageComponent, data: {page: '7'}},
  { path: 'metafun/**', component: HomeComponent, data: {page: '10'}},
  { path: '**', component: HomeComponent, data: {page : '11'}}
];

export const APP_ROUTES = RouterModule.forRoot( appRoutes );
