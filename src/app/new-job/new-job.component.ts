import { Component, OnInit } from '@angular/core';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {CdkDrag, CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Router} from '@angular/router';
import {checkPort} from '@angular-devkit/build-angular/src/angular-cli-files/utilities/check-port';


@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css']
})

export class NewJobComponent implements OnInit {

  private user = undefined;
  public resps = [];
  allFolders;
  private allFiles = [];
  public allNamesFiles = [];
  public csvNamesFiles = [];
  public ddfNamesFiles = [];
  public homeFolderFiles = [];
  public metaMethodS = '';
  public funcharS = '';
  public ontologiesS = [];
  public propagateS = '';
  public refOrganismS = '';
  public ddfNamesFilesS = [];
  public designS = '';
  public designValuesS = [];
  public designValuesSS = [];
  public designValuesSSS = [];
  public designValuesSSSS = [];


  constructor(private cookieService: CookieService, private router: Router) {
  }

  ngOnInit() {
    this.loadUserFiles();
  }

  containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()+\-=\[\]{};':"\\|,.<>\/?~àáâãäåæçèéêëìíîïñòóôõöœùúûüýÿÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÑÒÓÔÕÖŒÙÚÛÜÝŸ]/;
    return specialChars.test(str);
  }

  async loadUserFiles() {
    this.allFolders = '';
    this.allFiles = [];
    this.allNamesFiles = [];
    this.csvNamesFiles = [];
    this.ddfNamesFiles = [];
    this.homeFolderFiles = [];
    this.metaMethodS = '';
    this.funcharS = '';
    this.ontologiesS = [];
    this.propagateS = '';
    this.refOrganismS = '';
    this.ddfNamesFilesS = [];
    this.designS = '';
    this.designValuesS = [];
    this.designValuesSS = [];
    this.designValuesSSS = [];
    this.designValuesSSSS = [];

    const api = new ApiUtils('files');
    this.user = this.cookieService.get('userLoged');
    const fol = [];
    if (this.user !== '') {
      this.allFiles = JSON.parse(await api.getFilesFromUser(this.user));
      const compareCombo = document.getElementById('compareOptionsCombo');
      const launchButton = document.getElementById('launchButton');
      launchButton.addEventListener('click', (e: Event) => this.getLaunchOptions());
      compareCombo.addEventListener('change', (e: Event) => this.comboChanged());
      this.allFolders = await api.getUserFolders(this.user);
      this.allFolders = JSON.parse(this.allFolders);
      const foldersPaths = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0 ; i < this.allFiles.length; i++) {
        if (this.allFiles[i].parentDir.match('files/') && this.allFiles[i].parentDir.length === 6 && !this.allFiles[i].type.match('FOLDER')) {
            this.homeFolderFiles.push(this.allFiles[i]);
        }
        if (this.allFiles[i].type.match('FOLDER')) {
          fol.push(this.allFiles[i].name);
          foldersPaths.push(this.allFiles[i].parentDir + this.allFiles[i].name);
        }
      }
      let fileVal = '';
      let iconVal = '';
      let fileParsed;
      for (let i = 0 ; i < fol.length; i ++) {
        this.allNamesFiles.push({value: fol[i], icon: 'folder_open', disabled: true, file: null});
        const foldercontentfiles = await api.getFolderContent(this.user, foldersPaths[i]);
        if (foldercontentfiles.length > 0) {
          fileParsed = JSON.parse(foldercontentfiles);
          fileParsed = fileParsed.sort((a, b) => (a.name > b.name) ? 1 : -1);
          for (let j = 0; j < fileParsed.length; j++) {
            iconVal = 'none';
            fileParsed[j].name.substring(fileParsed[j].name.length - 3,
                fileParsed[j].name.length).match('csv') ? iconVal = 'assignment' : iconVal = 'note_alt';
            // fileParsed[j].name.substring(fileParsed[j].name.length - 3,
            //     fileParsed[j].name.length).match('tsv') ? iconVal = 'note_alt' : iconVal = 'none';
            // @ts-ignore
            if (!fileParsed[j].name.match('help.txt') && !iconVal.match('none')) {
                this.allNamesFiles.push({value: fileParsed[j].name, icon: iconVal, file: null});
            }
          }
        }
        /*for (let j = 0 ; j < this.allFolders[fol[i]].length ; j++) {
          fileVal = this.allFolders[fol[i]][j];
          // @ts-ignore
          fileVal.substring(fileVal.length - 3, fileVal.length).match('csv') ? iconVal = 'assignment' : iconVal = 'note_alt';
          // @ts-ignore
          if (!fileVal.match('help.txt')) {
            this.allNamesFiles.push({value: fileVal, icon: iconVal, file: null});
          }
        }*/
      }

      this.allNamesFiles.push({value: 'Home', icon: 'folder_open', disabled: true});
      let iconValue = 'note_alt';
      for (let i = 0; i < this.homeFolderFiles.length; i++) {
          iconValue = this.homeFolderFiles[i].name.match('csv') ? iconVal = 'assignment' : iconVal = 'note_alt';
          this.allNamesFiles.push({value: this.homeFolderFiles[i].name,
              icon: iconVal,
              file: this.homeFolderFiles[i]
            });
      }
      for (let i = 0; i < this.allFiles.length; i++) {
        for (let j = 0; j < this.allNamesFiles.length; j++) {
          if (this.allFiles[i].name.match(this.allNamesFiles[j].value)) {
            this.allNamesFiles[j].file = this.allFiles[i];
          }
        }
      }
      console.log (this.homeFolderFiles);
      console.log (this.allNamesFiles);
    } else {
      this.router.navigate(['metafun']);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    document.getElementById('compareOptionsCombo').addEventListener('change', (e: Event) => this.comboChanged());
    if (this.ddfNamesFiles.length <= 7 && this.csvNamesFiles.length <= 7 || (!event.previousContainer.element.nativeElement.getAttribute('id').match('allList'))) {
      if (event.previousContainer === event.container) {
        if (!event.previousContainer.element.nativeElement.getAttribute('id').match('allList')) {
          moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        if ((this.ddfNamesFiles.length === this.csvNamesFiles.length) && (this.ddfNamesFiles.length >= 2)) {
          this.checkExperimentalDesign();
        }
      } else {
        transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
        // @ts-ignore
        if ((this.ddfNamesFiles.length === this.csvNamesFiles.length) && (this.ddfNamesFiles.length >= 2)) {
          this.checkExperimentalDesign();
        }
      }
    }
  }

  omit_special_char(event) {
     let k;
     k = event.charCode;  //         k = event.keyCode;  (Both can be used)
     return((k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32 || (k >= 48 && k <= 57));
  }

  checkEmptyTextbox() {
    const jobNamein: HTMLInputElement = document.getElementById('jobNameInput') as HTMLInputElement;
    const jobButton: HTMLButtonElement = document.getElementById('launchButton') as HTMLButtonElement;
    const jobN = jobNamein.value.trim();
    if (jobN.length > 0) {
      jobButton.disabled = false;
    } else {
      jobButton.disabled = true;
    }

  }

  async checkExperimentalDesign() {
    const api = new ApiUtils('files');
    const experimentalDiv = document.getElementById('experimentalDiv');
    const responses = [];
    this.resps = responses;
    for (let i = 0; i < this.ddfNamesFiles.length; i++) {
      const resp = await api.checkExperimentalDesign(this.user, this.csvNamesFiles[i].file.path, this.ddfNamesFiles[i].file.path);
      responses.push(resp);
    }

    let flag = true;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < responses.length; i++) {
      if (responses[i].match('false')) {
        flag = false;

      }
    }
    if (flag === true) {
      this.fillExperimentalDesignTable();
    } else {
      experimentalDiv.innerHTML =
        '<div class="col s12 center metaBackground">' +
        '<select class="browser-default" id="compareOptionsCombo">' +
        '<option>Case - Control</option><option >(Case Female - Control Female) - (Case Male - Control Male)</option></select>' +
        '<div><table  id="compareOptionsTable">' +
        '<thead id="optionsTableHead"><th> CSV File </th><th> Case </th><th> Control </th></thead>' +
        '<tbody id="compareOptionsTableBody"></tbody></table></div></div></div>';
    }
  }

  async comboChanged() {
    const compareCombo = document.getElementById('compareOptionsCombo') as HTMLOptionElement;
    const optTHead = document.getElementById('optionsTableHead');
    const api = new ApiUtils('files');
    const tableBody = document.getElementById('compareOptionsTableBody');
    let newtable = '';
    tableBody.innerHTML = '';
    let tags = '';
    let k = 0;
    if (compareCombo.value.match('CS')) {
      optTHead.innerHTML = '<th class="center">\n' +
        '                CSV File\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Case\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Control\n' +
        '              </th>';

      for (let i = 0; i < this.ddfNamesFiles.length; i++) {
        tags = await api.getExperimentalDesign(this.user, this.ddfNamesFiles[i].file.parentDir + '' + this.ddfNamesFiles[i].file.name);
        const tagsR = tags.split(';');
        let sel = '<select class="browser-default" name="edSelect" id="edSelect_' + k + '">';
        let sel2  = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 1) + '">';
        for (let j = 0; j < tagsR.length - 1; j++) {
          sel += '<option>' + tagsR[j] + '</option>';
          sel2 += '<option>' + tagsR[j] + '</option>';
        }
        sel += '</select>';
        sel2 += '</select>';
        newtable +=
          '<tr>' +
          ' <td>' + this.csvNamesFiles[i].file.name + '</td>' +
          ' <td>' + sel + '</td>' + '<td>' + sel2 + '</td>' +
          '</tr>';
        k += 2;
      }
    } else if (!compareCombo.value.match('CS')) {
      optTHead.innerHTML = '<th>\n' +
        '                CSV File\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Case Female\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Control Female\n' +
        '              </th>' +
        '                <th class="center">\n' +
        '                  Case Male\n' +
        '                </th>\n' +
        '                <th class="center">\n' +
        '                  Control Male\n' +
        '                </th>';
      for (let i = 0; i < this.ddfNamesFiles.length; i++) {
        tags = await api.getExperimentalDesign(this.user, this.ddfNamesFiles[i].file.parentDir + '' + this.ddfNamesFiles[i].file.name);
        const tagsR = tags.split(';');
        let sel = '<select class="browser-default" name="edSelect" id="edSelect_' + k + '">';
        let sel2 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 1) + '">';
        let sel3 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 2) + '">';
        let sel4 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 3) + '">';
        for (let j = 0; j < tagsR.length - 1; j++) {
          sel += '<option>' + tagsR[j] + '</option>';
          sel2 += '<option>' + tagsR[j] + '</option>';
          sel3 += '<option>' + tagsR[j] + '</option>';
          sel4 += '<option>' + tagsR[j] + '</option>';
        }
        sel += '</select>';
        sel2 += '</select>';
        sel3 += '</select>';
        sel4 += '</select>';
        newtable +=
          '<tr>' +
          ' <td>' + this.csvNamesFiles[i].file.name + '</td>' +
          ' <td>' + sel + '</td>' + '<td>' + sel2 + '</td>' + '<td>' + sel3 + '</td>' + '<td>' + sel4 + '</td>' +
          '</tr>';
        k += 4;
      }
    }
    tableBody.innerHTML = newtable;
  }

  async fillExperimentalDesignTable() {
    const api = new ApiUtils('files');
    const tableBody = document.getElementById('compareOptionsTableBody');
    tableBody.innerHTML = '';
    let tags = '';
    let k = 0;
    for (let i = 0; i < this.ddfNamesFiles.length; i++) {
      tags = await api.getExperimentalDesign(this.user, this.ddfNamesFiles[i].file.parentDir + '' + this.ddfNamesFiles[i].file.name);
      const tagsR = tags.split(';');
      let sel = '<select class="browser-default" name="edSelect" id="edSelect_' + k + '">';
      let sel2 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 1) + '">';
      for (let j = 0; j < tagsR.length - 1; j++) {
        sel += '<option>' + tagsR[j] + '</option>';
        sel2 += '<option>' + tagsR[j] + '</option>';
      }
      sel += '</select>';
      sel2 += '</select>';
      tableBody.innerHTML +=
        '<tr>' +
        ' <td>' + this.csvNamesFiles[i].file.name + '</td>' +
        ' <td>' + sel + '</td>' + '<td>' + sel2 + '</td>' +
        '</tr>';
      k += 2;
    }
  }

  nextStep(index: number) {
      switch (index) {
          case 1:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              break;

          case 2:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              this.loadUserFiles();

              break;
          case 3:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              break;
          case 4:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              break;
          default:
              break;
      }
      this.updateOptions();
  }
  prevStep(index: number) {
      switch (index) {
          case 0:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              break;

          case 1:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              break;
          case 2:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp2').setAttribute('style',
                  'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              this.loadUserFiles();

              break;
          case 3:
              document.getElementById('step0Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step1Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step2Div').setAttribute('class', 'row col s10 offset-s2 center hide');
              document.getElementById('step3Div').setAttribute('class', 'row col s10 offset-s2 center ');
              // document.getElementById('experimentalDiv').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              // document.getElementById('clickBar').setAttribute('class', 'col s10 offset-s2 center metaBackground hide');
              document.getElementById('step4Div').setAttribute('class', 'row col s10 offset-s2 center hide');

              document.getElementById('stp0').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp1').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp2').setAttribute('style', 'font-size: 18px;');
              document.getElementById('stp3').setAttribute('style', 'font-size: 18px; color:#BC7836;  background-color:#F8E7C6; border-left: 10px solid #E38930;');
              document.getElementById('stp4').setAttribute('style', 'font-size: 18px;');
              break;
          default:
              break;
      }
      this.updateOptions();
    }

  getLaunchOptions() {
    const metaMethod: NodeList = document.getElementsByName('radioMethod');
    const funcChar: NodeList = document.getElementsByName('radioFunctional');
    const reference: NodeList = document.getElementsByName('radioReference');
    const propagate: NodeList = document.getElementsByName('radioPropagate');
    const bpCheck: HTMLInputElement = document.getElementById('checkBP') as HTMLInputElement;
    const mfCheck: HTMLInputElement = document.getElementById('checkMF') as HTMLInputElement;
    const ccCheck: HTMLInputElement = document.getElementById('checkCC') as HTMLInputElement;
    const jobNameInput: HTMLInputElement = document.getElementById('jobNameInput') as HTMLInputElement;
    const designAssociation = [];
    const selectedDesign = document.getElementById('compareOptionsCombo') as HTMLOptionElement;
    const design = 0;
    const designValues = [];
    const ops = [];
    const checked = [];
    const jobName =  jobNameInput.value.replace(/\s/g, '_');
    const binValue = (+bpCheck.checked * 100 + +mfCheck.checked * 10 + +ccCheck.checked).toString();
    const funcharValue = parseInt(binValue, 2);
    for (let i = 0; i < metaMethod.length; i++) {
      // @ts-ignore
      if (metaMethod[i].checked) {
        ops.push(i);
      }
    }

    for (let i = 0; i < funcChar.length; i++) {
      // @ts-ignore
      if (funcChar[i].checked) {
        ops.push(i);
      }
    }
    for (let i = 0; i < reference.length; i++) {
      // @ts-ignore
      if (reference[i].checked) {
        ops.push(i);
      }
    }
    let k = 0;
    if (selectedDesign.value.match('CS')) {
      k = 2;
      ops.push(0);
    } else {
      k = 4;
      ops.push(1);
    }

    for (let i = 0 ; i < propagate.length; i++) {
      // @ts-ignore
      if (propagate[i].checked) {
        ops.push(i);
      }
    }

    ops.push(funcharValue);
    for (let i = 0; i < this.ddfNamesFiles.length * k; i++) {
      designAssociation.push(document.getElementById('edSelect_' + i) as HTMLOptionElement);
    }
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < designAssociation.length; i++) {
      if (designAssociation[i] != null) {
        designValues.push(designAssociation[i].value);
      }
    }

    if (!this.containsSpecialChars(jobName) && this.csvNamesFiles.length >= 2) {
      this.launchJob(jobName, this.csvNamesFiles, this.ddfNamesFiles, designValues, ops);
    } else {
      let alerted = 0;
      if (alerted == 0) {
        alert('For the Meta-analysis to run, it needs at least two studies and its name can not contain special characters')
        alerted = 1;
      }
    }
  }

  launchJob(jobName, exprMatFiles, expDesignFiles, designValues, optionsArray) {
    const api = new ApiUtils('jobs');
    const edFiles = [];
    for (let i = 0 ; i < expDesignFiles.length ; i++) {
      edFiles.push(expDesignFiles[i].file);
    }

    const exprTech = [];
    let designString = '';

    let commandArgs = '';
    if (optionsArray[0] === 1) {
      commandArgs += '-FixedEffect';
    } else {
      commandArgs += '-RandomEffect';
    }

    if (optionsArray[1] === 1) {
      commandArgs += '-GSA'; // '-Hipathia';
    } else {
      commandArgs += ' -GSA';
    }

    if (optionsArray[2] === 0) {
      commandArgs += ' -HS';
    } else if (optionsArray[2] === 1) {
      commandArgs += ' -MM';
    } else if (optionsArray[2] === 2) {
      commandArgs += ' -RN';
    }

    if (optionsArray[3] === 0) {
      commandArgs += ' -CS';
    } else {
      commandArgs += ' -GENDER';
    }

    if (optionsArray[4] === 0 ) {
      commandArgs += ' -TRUE';
    } else {
      commandArgs += ' -FALSE';
    }

    commandArgs += ' -' + optionsArray[5];

    for (let i = 0 ; i < designValues.length ; i++) {
      designString += designValues[i] + '@';
    }

    for (let i = 0; i < exprMatFiles.length; i++) {
      exprTech.push(exprMatFiles[i].file.path + '#' + exprMatFiles[i].file.tech);
    }
    api.registerJob(this.user, jobName, exprTech, edFiles, designString, commandArgs);

    this.router.navigate(['metafun/userprofile']);
  }

  /** Predicate function that only allows even numbers to be dropped into a list. */
  isCSVPredicate(item: CdkDrag<string>) {
    // @ts-ignore
    if (item.data.icon.match('assignment')) {
      return true;
    }
    return false;
  }

  isDDFPredicate(item: CdkDrag<string>) {
    // @ts-ignore
    if (item.data.icon.match('note_alt')) {
      return true;
    }
    return false;
  }

  /** Predicate function that doesn't allow items to be dropped into a list. */
  noReturnPredicate() {
    return true;
  }

  updateOptions() {
      const metaMethod: NodeList = document.getElementsByName('radioMethod');
      const funcChar: NodeList = document.getElementsByName('radioFunctional');
      const reference: NodeList = document.getElementsByName('radioReference');
      const propagate: NodeList = document.getElementsByName('radioPropagate');
      const bpCheck: HTMLInputElement = document.getElementById('checkBP') as HTMLInputElement;
      const mfCheck: HTMLInputElement = document.getElementById('checkMF') as HTMLInputElement;
      const ccCheck: HTMLInputElement = document.getElementById('checkCC') as HTMLInputElement;
      const selectedDesign = document.getElementById('compareOptionsCombo') as HTMLOptionElement;
      this.ontologiesS = [];
      const designAssociation = [];

        // @ts-ignore
      this.metaMethodS = (metaMethod[0].checked) ? ('Random Effect Model') : ('Fixed Effect Model');
      // @ts-ignore

      this.funcharS = (funcChar[0].checked) ? ('GSA') : ('Hipathia');
      // @ts-ignore

      this.refOrganismS = (reference[0].checked) ? ('Homo Sapiens') : ((reference[1].checked) ? ('Mus Musculus') : ('Rattus Norvegicus'));
      // @ts-ignore
      this.propagateS = (propagate[0].checked) ? ('YES') : ('NO');
      if (bpCheck.checked) {
          this.ontologiesS.push('Biological Process');
      }
      if (mfCheck.checked) {
          this.ontologiesS.push(' Molecular Functions');
      }
      if (ccCheck.checked) {
          this.ontologiesS.push(' Cellular Components');
      }
      for (const i in this.ddfNamesFiles) {
          this.ddfNamesFilesS.push(this.ddfNamesFiles[i].file.name);
      }
      this.designS = (selectedDesign.value === 'CS') ? ('Case vs Control') : ('Sex Difference');
      const k = (selectedDesign.value === 'CS') ? (2) : (4);
      for (let i = 0; i < this.ddfNamesFiles.length * k; i++) {
          // @ts-ignore
          designAssociation.push(document.getElementById('edSelect_' + i) as HTMLOptionElement).value;
      }
      this.designValuesS = [];
      this.designValuesSS = [];
      this.designValuesSSS = [];
      this.designValuesSSSS = [];

      for (let i = 0; i < designAssociation.length; i += k) {
          if (designAssociation[i] != null) {
              this.designValuesS.push(designAssociation[i].value);
              this.designValuesSS.push(designAssociation[i + 1].value);
              if (k === 4) {
                  this.designValuesSSS.push(designAssociation[i + 2].value);
                  this.designValuesSSSS.push(designAssociation[i + 3].value);
              }
          }
      }
  }

}
