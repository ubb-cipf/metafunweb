import {Component, Input, OnInit} from '@angular/core';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {MenuComponent} from '../menu/menu.component';
import {NgxSmartModalService} from 'ngx-smart-modal';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  private _http: any;
  private message: any;
  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    private cookieService: CookieService,
    private router: Router
  ) {}
  jobs;
  diskusage;
  studies = [];
  user;

  Name: string;
  myFile: File; /* property of File type */

  @Input() selectedFile;


  fileChange(files: any) {
    const inFile = document.getElementById('myFile') as HTMLInputElement;
    this.myFile = inFile.files[0];
  }


  async ngOnInit() {
    const apiJobs = new ApiUtils('jobs');
    const apiFiles = new ApiUtils('files');
    let auxjobs;
    this.user = this.cookieService.get('userLoged');
    if (this.user !== '') {
      const rawJobs = await apiJobs.getJobsFromUser(this.user);
      let cadena = [];
      const files = [];
      auxjobs = JSON.parse(rawJobs);
      for (let i = 0; i < auxjobs.length ; i++) {
        cadena = auxjobs[i].exprFiles.split('@');
        cadena.pop();
        files.push(cadena);
      }
      auxjobs.sort((a , b) => {
        return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
      });
      this.jobs = auxjobs.reverse();
      const items = [];
      for (let i = 0; i < files.length; i++) {
        items[i] = [];
        for (let j = 0; j < files[i].length; j++) {
          items[i].push(files[i][j].split('#')[0].split('/')[files[i][j].split('#')[0].split('/').length - 1]);
        }
      }
      this.studies = items.reverse();
      const dus = await apiFiles.getDiskUsage(this.user);
      this.diskusage = 'You have ' + dus + ' in files';
      for (let i = 0 ; i < this.jobs.length ; i++) {
        const text = this.studies[i].toString();
        document.getElementById('studies' + i).innerText = text.replace(new RegExp(',', 'g'), '\n');
      }
    } else {
      this.router.navigate(['metafun']);
    }
  }
  
  printAnalysis() {
    //this.ngxSmartModalService.setModalData(analysis, 'infoModal', true);
    this.ngxSmartModalService.getModal('showJobInfoModal').open();
    for (let i = 0 ; i < this.jobs.length ; i++) {
      const text = this.studies[i].toString();
      document.getElementById('modalstudies' + i).innerText = text.replace(new RegExp(',', 'g'), '\n');
    }
  }

  checkFilesDelete() {
    const input: HTMLInputElement = document.getElementById('deleteFilesInput') as HTMLInputElement;
    const button: HTMLButtonElement = document.getElementById('deleteFilesButton') as HTMLButtonElement;
    const texto = input.value;
    if (texto.match('DELETE ALL FILES')) {
      button.disabled = false;
    } else {
      button.disabled = true;
    }
  }

  async deleteAllFiles () {
    const api = new ApiUtils('files');
    await api.deleteAllFiles(this.user);
  }

  checkJobsDelete() {
    const input: HTMLInputElement = document.getElementById('deleteJobsInput') as HTMLInputElement;
    const button: HTMLButtonElement = document.getElementById('deleteJobsButton') as HTMLButtonElement;
    const texto = input.value;
    if (texto.match('DELETE ALL JOBS')) {
      button.disabled = false;
    } else {
      button.disabled = true;
    }
  }

  async deleteAllJobs () {
    const api = new ApiUtils('jobs');
    await api.deleteAllJobs(this.user);
  }


  checkAccountDelete() {
    const input: HTMLInputElement = document.getElementById('deleteAccountInput') as HTMLInputElement;
    const button: HTMLButtonElement = document.getElementById('deleteAccountButton') as HTMLButtonElement;
    const texto = input.value;
    if (texto.match('DELETE ACCOUNT')) {
      button.disabled = false;
    } else {
      button.disabled = true;
    }
  }

  async deleteAccount() {
    const api = new ApiUtils('users');
    const token = this.cookieService.get('userToken')
    await api.deleteUser(token)

    this.router.navigate(['metafun']);
    this.logOut()
    
  }

  async deleteJob(index: number) {
    const api = new ApiUtils('jobs');
    let txt;
    if (confirm('You are about to delete the next Job and all the results of it: \n\n\t- ' + this.jobs[index].name + '\n\nAre you sure?')) {
      api.deleteJob(this.user, this.jobs[index].name);
      this.ngOnInit();
      txt = 'You pressed OK!';
    } else {
      txt = 'You pressed Cancel!';
    }
  }

  async changePassword() {
    const api = new ApiUtils('users')
    const oldPasswordInput: HTMLInputElement = document.getElementById('oldPassInput') as HTMLInputElement;
    const newPassInput1: HTMLInputElement = document.getElementById('newPassInput1') as HTMLInputElement;
    const newPassInput2: HTMLInputElement = document.getElementById('newPassInput2') as HTMLInputElement;

    const oldPasword = oldPasswordInput.value;
    const newPass1 = newPassInput1.value;
    const newPass2 = newPassInput2.value;
    if (newPass1 == newPass2) {
      const res = await api.logInRequest(this.cookieService.get('userMail'),CryptoJS.MD5(oldPasword))
      if (res) {
        api.changePasswordRequest(this.cookieService.get('userMail'), oldPasword, newPass1);
        alert ('Your password has been succesfully changed');
        oldPasswordInput.value = "";
        newPassInput1.value = "";
        newPassInput2.value = "";
        
      } else {
        alert ('Your password is not correct');
      }
    }else {
      alert ('Your new password does not match');
    }

  }

  async changeMail () {
   /*  const api = new ApiUtils('users')
    const passwordInput: HTMLInputElement = document.getElementById('password') as HTMLInputElement;
    const newEmail1Input: HTMLInputElement = document.getElementById('newEmail1') as HTMLInputElement;
    const newEmail2Input: HTMLInputElement = document.getElementById('newEmail2') as HTMLInputElement;
    
    const password = passwordInput.value;
    const newEmail1 = newEmail1Input.value;
    const newEmail2 = newEmail2Input.value;

    if (newEmail1 == newEmail2) {
      const res = await api.logInRequest(this.cookieService.get('userMail'),CryptoJS.MD5(password))
      if (res) {
        api.changePasswordRequest(this.cookieService.get('userMail'), oldPasword, newPass1);
        alert ('Your password has been succesfully changed');
        password.value = "";
        newPassInput1.value = "";
        newPassInput2.value = "";
        
      } else {
        alert ('Your password is not correct');
      }
    }else {
      alert ('Your new password does not match');
    }

 */
  }


  showJobsInfo() {
    document.getElementById('jobsInfoUserDiv').setAttribute('class', '');
    document.getElementById('fileBrowserUserDiv').setAttribute('class', 'hide');
    document.getElementById('filesli').setAttribute('class', '');
    document.getElementById('jobsli').setAttribute('class', 'active');
  }

  showfilesInfo() {
    document.getElementById('jobsInfoUserDiv').setAttribute('class', 'hide');
    document.getElementById('fileBrowserUserDiv').setAttribute('class', 'col s10 push-s2');
    document.getElementById('filesli').setAttribute('class', 'active');
    document.getElementById('jobsli').setAttribute('class', '');
  }

  logOut() {
    this.cookieService.delete('userLoged');
    this.cookieService.delete('userMail');
    this.cookieService.delete('userToken');
    this.router.navigate(['metafun']);
    MenuComponent.reloadLogIntext('LogIn');
    MenuComponent.reloadSignUpText('SignUp');
  }

  onSubmit(): void {
    const api = new ApiUtils('files');
    const a = document.getElementById('myFile') as HTMLInputElement;
    api.nguploadFile(a.files[0], this.user, a.files[0].name, 'FILE', 'NA', 'currentFolder', 'currentFolder');
  }
}
