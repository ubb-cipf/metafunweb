import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import ApiUtils from '../utils/ApiUtils';
import {saveAs} from '../../../node_modules/file-saver';

import {FileBrowserComponent} from '../file-browser/file-browser.component';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.css']
})
export class FileManagerComponent implements OnInit, OnChanges {
  user;
  @Input() selectedFile;
  filesel;
  constructor(public ngxSmartModalService: NgxSmartModalService, private cookieService: CookieService) { }
  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
  }

  ngOnChanges(changes: SimpleChanges) {
    const selefil: SimpleChange = changes.selectedFile;
    this.filesel = selefil.currentValue;
  }


  printFile(file) {
    this.ngxSmartModalService.setModalData(file, 'infoModal', true);
    this.ngxSmartModalService.getModal('infoModal').open();
  }

  async deleteFile() {
    const api = new ApiUtils('files');
    let txt;
    if (confirm('You are about to delete the next Job and all the results of it: \n\n\t- ' + this.ngxSmartModalService.getModalData('infoModal').name + '\n\nAre you sure?')) {
      api.deleteFile(this.ngxSmartModalService.getModalData('infoModal').user, this.ngxSmartModalService.getModalData('infoModal').path);
      txt = 'You pressed OK!';
    } else {
      txt = 'You pressed Cancel!';
    }
  }

  async downloadFile() {
    const api = new ApiUtils('files');
    const file = await api.downloadAny(this.ngxSmartModalService.getModalData('infoModal').user,
        this.ngxSmartModalService.getModalData('infoModal').path);
    const downFile = new File([file], this.ngxSmartModalService.getModalData('infoModal').name,
        {type: 'text/plain;charset=utf-8'});
    saveAs(downFile);
  }

  renameFile() {
    const api = new ApiUtils('files');
    api.renameFile(this.user, this.ngxSmartModalService.getModalData('infoModal'), 'llll.sfs');
  }

  bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Byte';
    const i = +(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
  }

  selecthaschanged() {
  }
}
